﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Práctica_I
{
	class Grid
	{
		private readonly PictureBox planoCartesiano;
		private readonly float x_y;
		public Grid(ref PictureBox planoCartesiano, ref float x_y)
		{
			this.planoCartesiano = planoCartesiano;
			this.x_y = x_y + 1;
			CreateGrid();
		}

		public void CreateGrid()
		{
			Bitmap grid = new Bitmap(planoCartesiano.Width, planoCartesiano.Height);
			using (Graphics g = Graphics.FromImage(grid))
			{
				Pen line = new Pen(Brushes.Black);
				Pen dash = new Pen(Brushes.Gray);
				dash.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
				// Eje de las X
				g.DrawLine(line, 0, planoCartesiano.Height / 2, planoCartesiano.Width, planoCartesiano.Height / 2);
				Font drawFont = new Font("Arial", 8);
				SolidBrush drawBrush = new SolidBrush(Color.Black);
				for (float i = planoCartesiano.Width / 2, j = planoCartesiano.Width / 2, k = 0; i > 0; i -= (planoCartesiano.Width / 2) / x_y, j += (planoCartesiano.Width / 2) / x_y, k++)
				{
					if (k != 0)
					{
						g.DrawLine(dash, i, 0, i, planoCartesiano.Width);
						g.DrawLine(dash, j, 0, j, planoCartesiano.Width);
						g.DrawString((-k).ToString(), drawFont, drawBrush, i, (planoCartesiano.Width / 2));
						g.DrawString(k.ToString(), drawFont, drawBrush, j, (planoCartesiano.Width / 2));
					}
					g.DrawLine(line, i, (planoCartesiano.Width / 2) - 5, i, (planoCartesiano.Width / 2) + 5);
					g.DrawLine(line, j, (planoCartesiano.Width / 2) - 5, j, (planoCartesiano.Width / 2) + 5);

					
				}
				// Eje de las Y
				g.DrawLine(line, planoCartesiano.Width / 2, 0, planoCartesiano.Width / 2, planoCartesiano.Height);
				for (float i = planoCartesiano.Height / 2, j = planoCartesiano.Height / 2, k = 0; i > 0; i -= (planoCartesiano.Height / 2) / x_y, j += (planoCartesiano.Height / 2) / x_y, k++)
				{
					if (k != 0)
					{
						g.DrawLine(dash, 0, i, planoCartesiano.Height, i);
						g.DrawLine(dash, 0, j, planoCartesiano.Height, j);

						g.DrawString(k.ToString(), drawFont, drawBrush, (planoCartesiano.Height / 2), i);
						g.DrawString((-k).ToString(), drawFont, drawBrush, (planoCartesiano.Height / 2), j);
					}
					g.DrawLine(line, (planoCartesiano.Height / 2) - 5, i, (planoCartesiano.Height / 2) + 5, i);
					g.DrawLine(line, (planoCartesiano.Height / 2) - 5, j, (planoCartesiano.Height / 2) + 5, j);

					
				}
				// Recuadro exterior
				Rectangle rect = new Rectangle(0, 0, planoCartesiano.Width - 1, planoCartesiano.Height - 1);
				g.DrawRectangle(line, rect);
				g.Dispose();
			}
			planoCartesiano.Image = grid;
		}

		public void PaintDot(float x, float y)
		{
			Bitmap grid = new Bitmap(planoCartesiano.Image);
			using (Graphics g = Graphics.FromImage(grid))
			{
				SolidBrush brush = new SolidBrush(Color.Blue);
				g.FillEllipse(brush, x-5, y-5, 10, 10);
				g.Dispose();
			}
			planoCartesiano.Image = grid;
		}
	}
}
