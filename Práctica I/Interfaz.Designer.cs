﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Práctica_I
{
	partial class PracticaForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PracticaForm));
			this.newBtn = new System.Windows.Forms.Button();
			this.loadAsbBtn = new System.Windows.Forms.Button();
			this.planoCartesianoPB = new System.Windows.Forms.PictureBox();
			this.actionsGB = new System.Windows.Forms.GroupBox();
			this.coordenatesLBX = new System.Windows.Forms.ListBox();
			this.relCooBTN = new System.Windows.Forms.Button();
			this.absCooBTN = new System.Windows.Forms.Button();
			this.yLabel = new System.Windows.Forms.Label();
			this.xLabel = new System.Windows.Forms.Label();
			this.saveBtn = new System.Windows.Forms.Button();
			this.loadRelBtn = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.planoCartesianoPB)).BeginInit();
			this.actionsGB.SuspendLayout();
			this.SuspendLayout();
			// 
			// newBtn
			// 
			this.newBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.newBtn.Location = new System.Drawing.Point(13, 13);
			this.newBtn.Name = "newBtn";
			this.newBtn.Size = new System.Drawing.Size(75, 23);
			this.newBtn.TabIndex = 0;
			this.newBtn.Text = "Nuevo";
			this.newBtn.UseVisualStyleBackColor = true;
			// 
			// loadAsbBtn
			// 
			this.loadAsbBtn.Location = new System.Drawing.Point(376, 14);
			this.loadAsbBtn.Name = "loadAsbBtn";
			this.loadAsbBtn.Size = new System.Drawing.Size(117, 23);
			this.loadAsbBtn.TabIndex = 1;
			this.loadAsbBtn.Text = "Cargar Absolutas";
			this.loadAsbBtn.UseVisualStyleBackColor = true;
			// 
			// planoCartesianoPB
			// 
			this.planoCartesianoPB.Image = ((System.Drawing.Image)(resources.GetObject("planoCartesianoPB.Image")));
			this.planoCartesianoPB.Location = new System.Drawing.Point(13, 43);
			this.planoCartesianoPB.Name = "planoCartesianoPB";
			this.planoCartesianoPB.Size = new System.Drawing.Size(600, 600);
			this.planoCartesianoPB.TabIndex = 2;
			this.planoCartesianoPB.TabStop = false;
			// 
			// actionsGB
			// 
			this.actionsGB.Controls.Add(this.coordenatesLBX);
			this.actionsGB.Controls.Add(this.relCooBTN);
			this.actionsGB.Controls.Add(this.absCooBTN);
			this.actionsGB.Location = new System.Drawing.Point(639, 43);
			this.actionsGB.Name = "actionsGB";
			this.actionsGB.Size = new System.Drawing.Size(174, 600);
			this.actionsGB.TabIndex = 3;
			this.actionsGB.TabStop = false;
			this.actionsGB.Text = "Coordenadas";
			// 
			// coordenatesLBX
			// 
			this.coordenatesLBX.FormattingEnabled = true;
			this.coordenatesLBX.ItemHeight = 15;
			this.coordenatesLBX.Location = new System.Drawing.Point(7, 52);
			this.coordenatesLBX.Name = "coordenatesLBX";
			this.coordenatesLBX.Size = new System.Drawing.Size(156, 529);
			this.coordenatesLBX.TabIndex = 3;
			// 
			// relCooBTN
			// 
			this.relCooBTN.Location = new System.Drawing.Point(88, 23);
			this.relCooBTN.Name = "relCooBTN";
			this.relCooBTN.Size = new System.Drawing.Size(75, 23);
			this.relCooBTN.TabIndex = 2;
			this.relCooBTN.Text = "Relativas";
			this.relCooBTN.UseVisualStyleBackColor = true;
			// 
			// absCooBTN
			// 
			this.absCooBTN.Location = new System.Drawing.Point(7, 23);
			this.absCooBTN.Name = "absCooBTN";
			this.absCooBTN.Size = new System.Drawing.Size(75, 23);
			this.absCooBTN.TabIndex = 1;
			this.absCooBTN.Text = "Absolutas";
			this.absCooBTN.UseVisualStyleBackColor = true;
			// 
			// yLabel
			// 
			this.yLabel.AutoSize = true;
			this.yLabel.Location = new System.Drawing.Point(305, 25);
			this.yLabel.Name = "yLabel";
			this.yLabel.Size = new System.Drawing.Size(14, 15);
			this.yLabel.TabIndex = 4;
			this.yLabel.Text = "Y";
			// 
			// xLabel
			// 
			this.xLabel.AutoSize = true;
			this.xLabel.Location = new System.Drawing.Point(619, 332);
			this.xLabel.Name = "xLabel";
			this.xLabel.Size = new System.Drawing.Size(14, 15);
			this.xLabel.TabIndex = 5;
			this.xLabel.Text = "X";
			// 
			// saveBtn
			// 
			this.saveBtn.Location = new System.Drawing.Point(94, 13);
			this.saveBtn.Name = "saveBtn";
			this.saveBtn.Size = new System.Drawing.Size(75, 23);
			this.saveBtn.TabIndex = 6;
			this.saveBtn.Text = "Guardar";
			this.saveBtn.UseVisualStyleBackColor = true;
			// 
			// loadRelBtn
			// 
			this.loadRelBtn.Location = new System.Drawing.Point(499, 14);
			this.loadRelBtn.Name = "loadRelBtn";
			this.loadRelBtn.Size = new System.Drawing.Size(114, 23);
			this.loadRelBtn.TabIndex = 7;
			this.loadRelBtn.Text = "Cargar Relativas";
			this.loadRelBtn.UseVisualStyleBackColor = true;
			// 
			// PracticaForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(827, 665);
			this.Controls.Add(this.loadRelBtn);
			this.Controls.Add(this.saveBtn);
			this.Controls.Add(this.xLabel);
			this.Controls.Add(this.yLabel);
			this.Controls.Add(this.actionsGB);
			this.Controls.Add(this.planoCartesianoPB);
			this.Controls.Add(this.loadAsbBtn);
			this.Controls.Add(this.newBtn);
			this.Name = "PracticaForm";
			this.Text = "Práctica I";
			((System.ComponentModel.ISupportInitialize)(this.planoCartesianoPB)).EndInit();
			this.actionsGB.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Button newBtn;
		private Button loadAsbBtn;
		private PictureBox planoCartesianoPB;
		private GroupBox actionsGB;
		private Button relCooBTN;
		private Button absCooBTN;
		private Label yLabel;
		private Label xLabel;
		private Button saveBtn;
		private ListBox coordenatesLBX;
		private Button loadRelBtn;
	}


}

