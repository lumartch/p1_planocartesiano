﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Práctica_I
{
	class Coordenates
	{
		public Coordenates(float x, float y) {
			X = x;
			Y = y;
		}

		public float X { get; set; }
		public float Y { get; set; }
		override
		public string ToString() {
			return X.ToString() + "," + Y.ToString();
		}
	}
}
