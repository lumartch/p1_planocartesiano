﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Práctica_I
{
	public partial class PracticaForm : Form
	{
		public PracticaForm()
		{
			InitializeComponent();
			grid = new Grid(ref planoCartesianoPB, ref x_y);
			absCooBTN.Enabled = false;
			relCooBTN.Enabled = false;
			planoCartesianoPB.Enabled = false;
			x_width = (planoCartesianoPB.Width / 2) / (x_y + 1);
			y_heigh = (planoCartesianoPB.Height / 2) / (x_y + 1);
			planoCartesianoPB.MouseClick += HandleNewDot;
			newBtn.Click += HandleNewFile;
			saveBtn.Click += HandleSaveFile;
			loadAsbBtn.Click += (sender, e) => HandleLoadFile(sender, e, true);
			loadRelBtn.Click += (sender, e) => HandleLoadFile(sender, e, false);
			absCooBTN.Click += HandleToAbs;
			relCooBTN.Click += HandleToRel;
		}
		private Grid grid;
		private float x_y = 10;
		private float x_width = 0;
		private float y_heigh = 0;
		private List<Coordenates> coordenates = new List<Coordenates>();
		void HandleNewDot(Object o, MouseEventArgs e)
		{
			float x = (e.X - planoCartesianoPB.Width / 2) / x_width;
			float y = -(e.Y - planoCartesianoPB.Height / 2) / y_heigh;
			coordenatesLBX.Items.Add("[" + x.ToString() + " : " + y.ToString() + "]");
			coordenates.Add(new Coordenates(x, y));
			grid.PaintDot(e.X, e.Y);
		}

		void HandleNewFile(Object o, EventArgs e) {
			absCooBTN.Enabled = false;
			relCooBTN.Enabled = true;
			planoCartesianoPB.Enabled = true;
			coordenatesLBX.Items.Clear();
			coordenates.Clear();
			grid.CreateGrid();
		}

		void ClearCoordenatesListBox() {
			coordenatesLBX.Items.Clear();
			foreach (Coordenates c in coordenates)
			{
				coordenatesLBX.Items.Add("[" + c.X.ToString() + " : " + c.Y.ToString() + "]");
			}
		}

		void AbsToRel() {
			List<Coordenates> list = new List<Coordenates>();
			list.Add(new Coordenates(coordenates[0].X, coordenates[0].Y));
			for (int i = 0; i < coordenates.Count - 1; i++)
			{
				list.Add(new Coordenates((-1 * coordenates[i].X) + coordenates[i + 1].X, (-1 * coordenates[i].Y) + coordenates[i + 1].Y));
			}
			coordenates.Clear();
			coordenates = list.GetRange(0, list.Count);
			ClearCoordenatesListBox();
		}

		void RelToAbs()
		{
			List<Coordenates> list = new List<Coordenates>();
			list.Add(new Coordenates(coordenates[0].X, coordenates[0].Y));
			for (int i = 0; i < coordenates.Count - 1; i++)
			{
				Coordenates c = new Coordenates(0, 0);
				list.Add(new Coordenates(list[i].X + coordenates[i + 1].X, list[i].Y + coordenates[i + 1].Y));
			}
			coordenates.Clear();
			coordenates = list.GetRange(0, list.Count);
			ClearCoordenatesListBox();
		}

		void HandleToRel(Object o, EventArgs e)
		{
			if (coordenates.Count > 0)
			{
				AbsToRel();
				absCooBTN.Enabled = true;
				relCooBTN.Enabled = false;
			}
		}
		void HandleToAbs(Object o, EventArgs e)
		{
			if (coordenates.Count > 0)
			{
				RelToAbs();
				absCooBTN.Enabled = false;
				relCooBTN.Enabled = true;
			}
		}

		void HandleLoadFile(Object o, EventArgs e, bool isAbs)
		{
			using OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Archivos de Texto (*.txt)|*.txt";
			openFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				string[] fin = File.ReadAllLines(openFileDialog.FileName);
				if (new FileInfo(openFileDialog.FileName).Length == 0)
				{
					MessageBox.Show("Se ha ingresado un archivo vacío", "¡Archivo vacío!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				List<Coordenates> list = new List<Coordenates>();
				foreach (string row in fin) {
					string[] r = row.Split(',');
					if (r.Length != 2) {
						MessageBox.Show("Se ha ingresado un archivo con formato incorrecto.", "¡Archivo Erroneo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					list.Add(new Coordenates(float.Parse(r[0]), float.Parse(r[1])));
				}
				coordenates.Clear();
				coordenates = list.GetRange(0, list.Count);
				grid.CreateGrid();
				
				if (!isAbs) {
					RelToAbs();
					foreach (Coordenates c in coordenates)
					{
						float x = (c.X * x_width + planoCartesianoPB.Width / 2);
						float y = (c.Y * y_heigh + planoCartesianoPB.Height / 2);
						grid.PaintDot(x, y);
					}
					AbsToRel();
					absCooBTN.Enabled = true;
					relCooBTN.Enabled = false;
					planoCartesianoPB.Enabled = true;
				}
				else { 
					foreach (Coordenates c in coordenates) {
						float x = (c.X*x_width + planoCartesianoPB.Width / 2);
						float y = (c.Y*y_heigh + planoCartesianoPB.Height / 2);
						grid.PaintDot(x, y);
					}
					absCooBTN.Enabled = false;
					relCooBTN.Enabled = true;
					planoCartesianoPB.Enabled = true;
				}
				ClearCoordenatesListBox();
			}
		}

		void HandleSaveFile(Object o, EventArgs e) {
			if (coordenates.Count == 0) {
				MessageBox.Show("El archivo está vacío, necesita ingresar coordenadas para poder guardar un archivo.", "Sin coordenadas.",
													MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			using SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "Archivo de Texto (*.txt)|*.txt";
			saveFileDialog.FilterIndex = 2;
			saveFileDialog.RestoreDirectory = true;

			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				using StreamWriter outputFile = new StreamWriter(saveFileDialog.FileName);
				foreach (Coordenates c in coordenates)
				{
					outputFile.WriteLine(c.ToString());
				}
				MessageBox.Show("El archivo ha sido guardado.", "Guardado exitoso.",
														MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}